// Copyright Claudio Mattera 2020.
// Distributed under the MIT License.
// See accompanying file License.txt, or online at
// https://opensource.org/licenses/MIT

use std::env;
use std::fs::File;
use std::io::{Read, BufRead, BufReader};

use log::*;

use env_logger;

use chrono::prelude::*;

use clap::{app_from_crate, crate_authors, crate_description, crate_name, crate_version,
    Arg, ArgMatches, SubCommand
};

use base64::encode as b64encode;

use regex::Regex;

pub mod error;
use crate::error::*;

fn main() {
    match inner() {
        Ok(_) => {}
        Err(error) => error!("Error: {}", error),
    }
}

fn inner() -> Result<(), Box<dyn std::error::Error>> {
    let matches = parse_command_line();

    setup_logging(matches.occurrences_of("v"));

    let sensor_id = matches.value_of("sensor").unwrap();
    let temperature = read_temperature(sensor_id)?;

    match matches.subcommand() {
        ("read", Some(_)) => {
            info!("Temperature is {} C", temperature);
        }
        ("read-and-store", Some(subcommand)) => {
            info!("Temperature is {} C", temperature);

            let influxdb_base_url = reqwest::Url::parse(&subcommand.value_of("influxdb-url").unwrap())?;
            let influxdb_username = subcommand.value_of("influxdb-username").unwrap();
            let influxdb_password = get_password_from_environment_variable()?;
            let influxdb_database = subcommand.value_of("influxdb-database").unwrap();
            let influxdb_measurement = subcommand.value_of("influxdb-measurement").unwrap();
            let influxdb_field = subcommand.value_of("influxdb-field").unwrap();
            let mut influxdb_tags: Vec<&str> = subcommand.values_of("influxdb-tag").unwrap().collect();
            influxdb_tags.push("sensor_type=DS18S20");

            let id_tag = format!("ds18s20_id={}", sensor_id);
            influxdb_tags.push(&id_tag);

            let mut client_builder = reqwest::blocking::ClientBuilder::new();
            if matches.is_present("ignore-certificates") {
                client_builder = client_builder.danger_accept_invalid_certs(true);
            }
            if let Some(ca_cert_path) = matches.value_of("ca-cert") {
                let mut buffer = Vec::new();
                File::open(ca_cert_path)?.read_to_end(&mut buffer)?;
                let ca_certificate = reqwest::Certificate::from_pem(&buffer)?;
                client_builder = client_builder.add_root_certificate(ca_certificate)
            }

            let client = client_builder.build()?;

            post_reading_to_influxdb(
                temperature,
                &influxdb_base_url,
                influxdb_username,
                &influxdb_password,
                influxdb_database,
                influxdb_measurement,
                influxdb_field,
                influxdb_tags,
                &client,
            )?;
        }
        _ => println!("{}", matches.usage()),
    }

    Ok(())
}

fn parse_command_line() -> ArgMatches<'static> {
    app_from_crate!()
        .after_help("Influxdb password is read from environment variable INFLUXDB_PASSWORD")
        .arg(
            Arg::with_name("v")
                .short("v")
                .long("verbose")
                .multiple(true)
                .help("Verbosity level"),
        )
        .arg(
            Arg::with_name("ca-cert")
                .long("ca-cert")
                .help("Additional CA certificate for HTTPS connections")
                .takes_value(true),
        )
        .arg(
            Arg::with_name("ignore-certificates")
                .long("ignore-certificates")
                .help("Ignore certificate validation for HTTPS"),
        )
        .arg(
            Arg::with_name("sensor")
                .long("sensor")
                .required(true)
                .help("DS18S20 sensor ID")
                .takes_value(true),
        )
        .subcommand(
            SubCommand::with_name("read")
                .about("Reads temperature from DS18S20 sensor")
        )
        .subcommand(
            SubCommand::with_name("read-and-store")
                .about("Reads temperature from DS18S20 sensor and stores it to an Influxdb server")
                .arg(
                    Arg::with_name("influxdb-url")
                        .long("influxdb-url")
                        .required(true)
                        .help("URL of Influxdb server")
                        .takes_value(true),
                )
                .arg(
                    Arg::with_name("influxdb-username")
                        .long("influxdb-username")
                        .required(true)
                        .help("Username for the Influxdb server")
                        .takes_value(true),
                )
                .arg(
                    Arg::with_name("influxdb-database")
                        .long("influxdb-database")
                        .required(true)
                        .help("Database name for the Influxdb server")
                        .takes_value(true),
                )
                .arg(
                    Arg::with_name("influxdb-measurement")
                        .long("influxdb-measurement")
                        .required(true)
                        .help("Influxdb measurement")
                        .takes_value(true),
                )
                .arg(
                    Arg::with_name("influxdb-field")
                        .long("influxdb-field")
                        .required(true)
                        .help("Influxdb field")
                        .takes_value(true),
                )
                .arg(
                    Arg::with_name("influxdb-tag")
                        .long("influxdb-tag")
                        .required(true)
                        .help("Tags for Influxdb measurement")
                        .takes_value(true)
                        .multiple(true),
                )
        )
        .get_matches()
}

fn setup_logging(verbosity: u64) {
    let default_log_filter = match verbosity {
        0 => "warn",
        1 => "info",
        2 => "info,temperature_ds18s20_influxdb_driver=debug",
        3 | _ => "debug",
    };
    let filter = env_logger::Env::default().default_filter_or(default_log_filter);
    env_logger::Builder::from_env(filter).init();
}

fn get_password_from_environment_variable() -> Result<String, Box<dyn std::error::Error>> {
    let password = env::var_os("INFLUXDB_PASSWORD")
        .ok_or_else(|| TemperatureError::new("Missing environment variable INFLUXDB_PASSWORD".to_string()))?
        .into_string()
        .map_err(|_| {
            TemperatureError::new("Invalid INFLUXDB_PASSWORD environment variable content".to_string())
        })?;

    debug!("Removing INFLUXDB_PASSWORD from environment");
    env::remove_var("INFLUXDB_PASSWORD");

    Ok(password)
}

fn read_temperature(
        sensor_id: &str,
    ) -> Result<f64, Box<dyn std::error::Error>> {
    info!("Reading sensor {}", sensor_id);
    let sensor_path = format!("/sys/bus/w1/devices/{}/w1_slave", sensor_id);

    let file = File::open(sensor_path)?;
    parse_temperature(file)
}

fn parse_temperature<R: Read>(
        file: R,
    ) -> Result<f64, Box<dyn std::error::Error>> {
    let first_line_pattern = Regex::new(r".*: crc=(\w\w) (?P<valid>YES|NO)").unwrap();
    let second_line_pattern = Regex::new(r".*t=(?P<temperature>-?\d+)").unwrap();

    let reader = BufReader::new(file);

    let mut lines = reader.lines();
    let first_line: String = lines.next().unwrap()?;
    let second_line: String = lines.next().unwrap()?;
    let first_captures = first_line_pattern.captures(&first_line);
    let second_captures = second_line_pattern.captures(&second_line);

    match (first_captures, second_captures) {
        (Some(first_captures), Some(second_captures)) => {
            if first_captures.name("valid").unwrap().as_str() == "YES" {
                let temperature_string = second_captures.name("temperature").unwrap().as_str();
                let temperature = temperature_string.parse::<f64>()? / 1000.0;
                Ok(temperature)
            } else {
                Err(TemperatureError::new("CRC not valid".to_string()))?
            }
        },
        _ => {
            Err(TemperatureError::new("Invalid output".to_string()))?
        }
    }
}

fn post_reading_to_influxdb(
        temperature: f64,
        base_url: &reqwest::Url,
        username: &str,
        password: &str,
        database: &str,
        measurement: &str,
        field: &str,
        tags: Vec<&str>,
        client: &reqwest::blocking::Client
    ) -> Result<(), Box<dyn std::error::Error>> {
    info!("Posting traffic to Influxdb");
    let url = base_url.join(&format!("/write?db={}", database))?;
    let now: DateTime<Utc> = Utc::now();
    let body = create_influxdb_line(temperature, now, measurement, field, &tags);
    let encoded = b64encode(format!("{}:{}", username, password).as_bytes());
    let authorization = format!("Basic {}", encoded);
    debug!("Body: {}", &body);
    let request = client.post(url)
        .header(reqwest::header::AUTHORIZATION, authorization)
        .body(body)
        .build()?;
    debug!("Sending request: {:?}", request);
    client.execute(request)?
        .error_for_status()?;

    Ok(())
}

fn create_influxdb_line(
        temperature: f64,
        datetime: DateTime<Utc>,
        measurement: &str,
        field: &str,
        tags: &[&str]
    ) -> String {
    let tags = if tags.len() > 0 {
            format!(",{}", tags.join(","))
        } else {
            "".to_string()
        };
    format!(
        "{measurement}{tags} {field}={value} {timestamp}",
        measurement=measurement,
        tags=tags,
        field=field,
        value=temperature,
        timestamp=datetime.timestamp_nanos(),
    )
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn parse_file_positive_test() -> Result<(), Box<dyn std::error::Error>> {
        let file_content = "b3 01 4b 46 7f ff 0d 10 9a : crc=9a YES
b3 01 4b 46 7f ff 0d 10 9a t=27187";
        let file_content_raw = file_content.as_bytes();
        let temperature: f64 = parse_temperature(file_content_raw)?;
        let expected: f64 = 27.187;
        assert_eq!(temperature, expected);
        Ok(())
    }

    #[test]
    fn parse_file_negative_test() -> Result<(), Box<dyn std::error::Error>> {
        let file_content = "b3 01 4b 46 7f ff 0d 10 9a : crc=9a YES
b3 01 4b 46 7f ff 0d 10 9a t=-27187";
        let file_content_raw = file_content.as_bytes();
        let temperature: f64 = parse_temperature(file_content_raw)?;
        let expected: f64 = -27.187;
        assert_eq!(temperature, expected);
        Ok(())
    }

    #[test]
    fn parse_file_nocrc_test() -> Result<(), Box<dyn std::error::Error>> {
        let file_content = "b3 01 4b 46 7e ff 0d 10 9a : crc=9a NO
b3 01 4b 46 7f ff 0d 10 9a t=27187";
        let file_content_raw = file_content.as_bytes();
        let temperature = parse_temperature(file_content_raw);
        assert!(temperature.is_err(), "Should fail if CRC is invalid");
        Ok(())
    }

    #[test]
    fn parse_file_failure_test() -> Result<(), Box<dyn std::error::Error>> {
        let file_content = "b3 01 4b 46 7e ff 0d 10 9a : crc=9a NO
b3 01 4b 46 7f ff 0d 10 7187";
        let file_content_raw = file_content.as_bytes();
        let temperature = parse_temperature(file_content_raw);
        assert!(temperature.is_err(), "Should fail if input is invalid");
        Ok(())
    }

    #[test]
    fn create_influxdb_line_test() {
        let temperature: f64 = 12.4;
        let timestamp: DateTime<Utc> = Utc.ymd(2020, 3, 21).and_hms(8, 57, 15);
        let measurement: &str = "indoor_environment";
        let field: &str = "temperature";
        let tags: Vec<&str> = vec!("country=dk", "city=Odense");

        let line = create_influxdb_line(temperature, timestamp, measurement, field, &tags);
        let expected = "indoor_environment,country=dk,city=Odense temperature=12.4 1584781035000000000";
        assert_eq!(line, expected);
    }
}
