// Copyright Claudio Mattera 2020.
// Distributed under the MIT License.
// See accompanying file License.txt, or online at
// https://opensource.org/licenses/MIT

use std::fmt;

#[derive(Debug)]
pub struct TemperatureError(String);

impl TemperatureError {
    pub fn new(message: String) -> Self {
        Self(message)
    }
}

impl std::error::Error for TemperatureError {
    fn description(&self) -> &str {
        &self.0
    }
}

impl fmt::Display for TemperatureError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.0)
    }
}

impl From<std::io::Error> for TemperatureError {
    fn from(error: std::io::Error) -> Self {
        TemperatureError(error.to_string())
    }
}

impl From<std::num::ParseIntError> for TemperatureError {
    fn from(error: std::num::ParseIntError) -> Self {
        TemperatureError(error.to_string())
    }
}

impl From<reqwest::Error> for TemperatureError {
    fn from(error: reqwest::Error) -> Self {
        TemperatureError(error.to_string())
    }
}

impl From<reqwest::header::ToStrError> for TemperatureError {
    fn from(error: reqwest::header::ToStrError) -> Self {
        TemperatureError(error.to_string())
    }
}
